#ifndef VEC2_H
#define VEC2_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include "math.h"

class Vec2: public sf::Vector2f {
public:
    Vec2(): sf::Vector2f() {};
    Vec2(sf::Vector2f v): sf::Vector2f(v) {};
    Vec2(float x, float y): sf::Vector2f(x,y) {};
    float length();
    Vec2 normalize();
    float dot(Vec2 v);
};



#endif //VEC2_H