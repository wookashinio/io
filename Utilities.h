//
// Created by viktor on 12.05.2020.
//

#ifndef IO_UTILITIES_H
#define IO_UTILITIES_H

#include "Config.h"
#include "Vec2.h"

class Utilities {
public:
  static inline float colisionCheck(double d) {
    double result = d;
    /*TODO*/
    return d;
  }

  /** Pozycja piłki o zadanych parametrach po upływie czasu
   * Przed wywołaniem ustalić ball_touched = fallse*/
  static Vec2 dynamicPosition(Vec2 start, int time, Vec2 velocity, float friction);

  /** Jak daleko zawodnik sięga za podany czas*/
  static inline float coverableDistance(int time) {
    return ACCELERATION * time;
  };

  /** Pozycja do kopnięcia w bramkę piłki*/
  static Vec2 shootPosition(Vec2 ball, Side side);

  /** Pozycja pomiędzy piłką i swoją bramką*/
  static Vec2 defensePosition(Vec2 ball, Side side);

  /** Wyznacza kto z dwóch zawodników pierwszy dotrze do piłki*/
  static bool firstTooBall(Vec2 first, Vec2 second, Vec2 ball_start, Vec2 velocity, float friction);
};

#endif //IO_UTILITIES_H
