#include "Goal.h"

void Goal::draw(sf::RenderWindow& window) {
    window.draw(rectangle);
}

bool Goal::isGoal(Mover& ball) {
    sf::Vector2 ballPosition = ball.getPosition();
    float radius = ball.getRadius();

    if (position.x - radius < ballPosition.x &&
        ballPosition.x < position.x + radius + size.x &&
        position.y - radius < ballPosition.y &&
        ballPosition.y < position.y + radius + size.y) {
        return true;
    } else { 
        return false;
    }
}