# Opis konfiguracji pliku Config.json

## Field

Pola opisujące boisko

### height, width

Wysokość i szerokość boiska, w pikselach

### ball

Początkowa pozycja piłki na boisku, w pikselach

- x
- y

### goal

- height, width - rozmiar bramek w pikselach
- n_x, n_y - lewy, górny róg północnej bramki
- s_x, s_y - lewy, górny róg południowej bramki

### players

- 1_x, 1_y - pozycja początkowa pierwszego gracza, w pikselach
- 2_x, 2_y - pozycja początkowa drugiego gracza, w pikselach

## rules

- max_goals - maksymalna liczba bramek w jednym meczu
- max_time - maksymalny czas meczu, w minutach