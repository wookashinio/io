//
// Created by kuba on 02.05.20.
//

#ifndef IO_GAME_H
#define IO_GAME_H
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <memory>
#include <ctime>
#include "Mover.h"
#include "Goal.h"
#include "Config.h"
#include "Bot.h"


class Game {
public:
  Game(std::string &s, Mover &player, Mover &mov_ball) : name(s), ball(mov_ball),
                                                         goalN(GOAL_N_X, GOAL_N_Y, GOAL_WIDTH, GOAL_HEIGHT),
                                                         goalS(GOAL_S_X, GOAL_S_Y, GOAL_WIDTH, GOAL_HEIGHT),
                                                         goalsN(0), goalsS(0) {
    mover_vec.push_back(std::make_shared<Mover>(player));
    clock = sf::Clock();
    players_number = 1;
    secPlayer = false;
    startTime = std::time(nullptr);
    maxTime = MAX_TIME * 60;
  };

  void play(sf::RenderWindow &window, sf::Sprite &sprite);

  void addBot(Dummy &mover);
   void addPlayer(Mover& mover) {
     secPlayer = true;
     mover_vec.push_back(std::make_shared<Mover>(mover));
     players_number++;
   }
private:
    std::string name;
    int players_number;
    std::vector<std::shared_ptr<Mover>> mover_vec;
    sf::Clock clock;
    Mover ball;

    Goal goalN, goalS;
    int goalsN, goalsS;
    uint64_t maxTime;
    bool secPlayer;
    std::time_t startTime;

    void updateMovers(sf::RenderWindow& window);
};


#endif //IO_GAME_H
