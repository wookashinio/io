#ifndef IO_BOT_H
#define IO_BOT_H

#include "Mover.h"

class Dummy : public Mover {
public:
  Dummy(enum Side s, Mover &b, float x, float y, float a) : side(s), ball(b), Mover(x, y, a) {}

  Dummy(enum Side s, Mover &b, float x, float y) : side(s), ball(b), Mover(x, y) {}

  enum Side side;

  void getToPosition(Vec2 position);

  void kicking();

  void play();

private:
  Mover &ball;
};

#endif //IO_BOT_H
