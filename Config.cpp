#include "Config.h"

void configFromJson(void) {
    boost::property_tree::read_json(CONFIG_FILE, configSetup);
    FIELD_HEIGHT    = configSetup.get<int>("field.height");
    FIELD_WIDTH     = configSetup.get<int>("field.width");
    BALL_X          = configSetup.get<int>("field.ball.x");
    BALL_Y          = configSetup.get<int>("field.ball.y");
    GOAL_N_X        = configSetup.get<int>("field.goal.n_x");
    GOAL_N_Y        = configSetup.get<int>("field.goal.n_y");
    GOAL_S_X        = configSetup.get<int>("field.goal.s_x");
    GOAL_S_Y        = configSetup.get<int>("field.goal.s_y");
    GOAL_HEIGHT     = configSetup.get<int>("field.goal.height");
    GOAL_WIDTH      = configSetup.get<int>("field.goal.width");
    MAX_GOALS       = configSetup.get<int>("rules.max_goals");
    MAX_TIME        = configSetup.get<int>("rules.max_time");
    PLAYER_1_X      = configSetup.get<int>("field.players.1_x");
    PLAYER_1_Y      = configSetup.get<int>("field.players.1_y");
    PLAYER_2_X      = configSetup.get<int>("field.players.2_x");
    PLAYER_2_Y      = configSetup.get<int>("field.players.2_y");
}