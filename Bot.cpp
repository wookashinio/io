#include "Bot.h"

void Dummy::getToPosition(Vec2 position) {
  double buffer;

  buffer = this->side == N ? -1 : 1;

  while (this->position.x != position.x && this->position.y != position.y + buffer) {
    if (this->position.x > position.x) {
      this->applyForce({-ACCELERATION, 0});
    } else if (this->position.x < position.x) {
      this->applyForce({ACCELERATION, 0});
    } else {
      this->applyForce({-this->velocity.x, 0});
    }

    if (this->position.y > position.y + buffer) {
      this->applyForce({0, -ACCELERATION});
    } else if (this->position.y < position.y + buffer) {
      this->applyForce({0, ACCELERATION});
    } else {
      this->applyForce({0, -this->velocity.y});
    }
  }
}

void Dummy::kicking() {
  this->kick(this->ball);
}

void Dummy::play() {
  int time;
  Vec2 goTo, ballStart, ballCurr, dstVec;
  const float ballFricktion = this->ball.getFriction();

  ballStart = this->ball.getPosition();

  while (!end) {
    time = 0;
    ballCurr = this->ball.getPosition();
    dstVec = this->position - this->ball.getPosition();

    while (Utilities::coverableDistance(time) < dstVec.length() - BALL_R - PLAYER_R) {
      time++;
      ballCurr = Utilities::dynamicPosition(ballStart, time, this->ball.getVelocity(), ballFricktion);
    }

    goTo = Utilities::shootPosition(ballCurr, this->side);

    this->getToPosition(goTo);

    dstVec = this->ball.getPosition() - this->position;

    if (dstVec.length() < BALL_R + PLAYER_R + 5) {
      kicking();
    }
  }
}