#include "Mover.h"


void Mover::draw(sf::RenderWindow& window) {
    if(kicking) {
        circle.setFillColor(sf::Color(65, 65, 65));
    } else {
        circle.setFillColor(sf::Color(255, 255, 255));
    }

    window.draw(circle);
}

void Mover::update() {
    position += velocity;
    velocity *= friction;
    circle.setPosition(position);
}

void Mover::applyForce(sf::Vector2f force) {
    velocity += force;
}

void Mover::checkBoundaryCollision(sf::RenderWindow& window) {
    float width = window.getSize().x;
    float height = window.getSize().y;

    if (position.x > width-radius) {
        position.x = width-radius;
        velocity.x *= -1;
    } else if (position.x < radius) {
        position.x = radius;
        velocity.x *= -1;
    } else if (position.y > height-radius) {
        position.y = height-radius;
        velocity.y *= -1;
    } else if (position.y < radius) {
        position.y = radius;
        velocity.y *= -1;
    }
}

void Mover::kick(Mover& ball) {

    kicking = true;
    Vec2 distanceVect = ball.position - position;
    float distanceVectMag = distanceVect.length();
    float minDistance = radius + ball.radius;

    if(distanceVectMag < minDistance + 10) {
        Vec2 normal = distanceVect.normalize() * (float)0.2;
        ball.applyForce(normal);
        kicking = false;
    }
}


void Mover::checkCollision(Mover& other) {

    Vec2 distanceVect = other.position - position;

    float distanceVectMag = distanceVect.length();
    float minDistance = radius + other.radius;

    if(distanceVectMag < minDistance) {

        float differenceLen = minDistance - distanceVectMag;

        Vec2 normal = distanceVect.normalize();

        Vec2 rv = other.velocity - velocity;

        float velAlongNormal = rv.dot(normal);

        // Do not resolve if velocities are separating
        if(velAlongNormal > 0)
            return;

        // Calculate restitution
        float e = std::min( restitution, other.restitution);

        // Calculate impulse scalar
        float j = -e * velAlongNormal;
        j /= (1 / mass + 1 / other.mass);


        // Apply impulse
      Vec2 impulse = j * normal;
      velocity -= 1 / mass * impulse;
      other.velocity += 1 / other.mass * impulse;
      position.x -= sqrt(differenceLen) * normal.x;
      position.y -= sqrt(differenceLen) * normal.y;
    }

}

void Mover::setVelocity(sf::Vector2f vec) {
  velocity = vec;
};

const Vec2 &Mover::getPosition() const {
  return position;
}

const Vec2 &Mover::getVelocity() const {
  return velocity;
}

const float &Mover::getFriction() const {
  return friction;
}

const float &Mover::getRadius() const {
  return radius;
}

void Mover::resetToPosition(float x, float y) {
    position.x = x;
    position.y = y;
    velocity.x = 0;
    velocity.y = 0;
    circle.setPosition(position);
}