#ifndef GOAL_H
#define GOAL_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "Mover.h"
#include "Vec2.h"

class Goal {
    public:
        Goal(float x, float y, float sizeX, float sizeY): 
        position(x, y), size(sizeX, sizeY), rectangle(sf::Vector2f(sizeX, sizeY)) {
            rectangle.setPosition(x, y);
            rectangle.setFillColor(sf::Color(255, 255, 255));
        };

        void draw(sf::RenderWindow &window);

        bool isGoal(Mover &ball);

    private:
        sf::RectangleShape rectangle;

    protected:
        Vec2 position;
        Vec2 size;
};

#endif //GOAL_H