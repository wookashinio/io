#ifndef MOVER_H
#define MOVER_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "Vec2.h"
#include "Config.h"
#include "Utilities.h"

class Mover {
public:
  Mover(float x, float y, float a) : position(x, y), velocity(0, 0), circle(a, a), radius(a) {
    circle.setOrigin(sf::Vector2f(a, a));
    mass = radius * radius / 100;
    friction = 1 - (mass / 10000);
  };

  Mover(float x, float y) : Mover(x, y, PLAYER_R) {};

  void update();

  void draw(sf::RenderWindow &window);

  void applyForce(sf::Vector2f force);

  void checkBoundaryCollision(sf::RenderWindow &window);

  void checkCollision(Mover &other);

  void setVelocity(sf::Vector2f vec);

  void kick(Mover &ball);

  const Vec2 &getPosition() const;

  const Vec2 &getVelocity() const;

  const float &getFriction() const;

  const float &getRadius() const;

  bool kicking = false;

  void resetToPosition(float x, float y);

private:
  sf::CircleShape circle;
  float mass;
  float restitution = 1;
protected:
  Vec2 position;
  Vec2 velocity;
  float radius;
  float friction;
};


#endif //MOVER_H
