//
// Created by viktor on 12.05.2020.
//

#ifndef IO_CONFIG_H
#define IO_CONFIG_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

static int FIELD_HEIGHT =   800;
static int FIELD_WIDTH  =   600;
static int PLAYER_1_X   =   400;
static int PLAYER_1_Y   =   50;
static int PLAYER_2_X   =   400;
static int PLAYER_2_Y   =   550;
#define PLAYER_R            50
static int BALL_X       =   400;
static int BALL_Y       =   300;
#define BALL_R              20
#define ACCELERATION        0.0005
static int GOAL_N_X     =   200;
static int GOAL_N_Y     =   0;
static int GOAL_S_X     =   200;
static int GOAL_S_Y     =   580;
static int GOAL_HEIGHT  =   20;
static int GOAL_WIDTH   =   400;
static int MAX_GOALS = 5;
static int MAX_TIME = 3;    // Czas w minutach
#define UNDEFINED           -1
#define CONFIG_FILE         "../Config.json"

enum Side {
  N, S
};

static bool ball_touched = false;
static bool end = false;
//static bool pause = false;

static boost::property_tree::ptree configSetup;

void configFromJson(void);

#endif //IO_CONFIG_H
