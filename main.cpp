#include <SFML/Graphics.hpp>
#include <iostream>
#include "Game.h"
#include "Config.h"


int main() {
  configFromJson();
  sf::VideoMode videomode = sf::VideoMode(FIELD_HEIGHT, FIELD_WIDTH);

  sf::RenderWindow window(videomode, "SFML works!");
  sf::Texture texture;
  texture.loadFromFile("../trava_za_teren.png");
  sf::Sprite sprite;
  sprite.setTexture(texture);
  sf::Vector2f targetSize(videomode.width, videomode.height);

  Mover player1(PLAYER_1_X, PLAYER_1_Y);
  Mover ball(BALL_X, BALL_Y, BALL_R);
  sprite.setScale(
          targetSize.x / sprite.getLocalBounds().width,
          targetSize.y / sprite.getLocalBounds().height);
  std::string st = "gra";
  Game game = Game(st, player1, ball);


  Mover player2(PLAYER_2_X, PLAYER_2_Y);
  game.addPlayer(player2);
  //  Dummy player2(S, ball, PLAYER_2_X, PLAYER_2_Y);
//    game.addBot(player2);
  game.play(window, sprite);

  return 0;
}