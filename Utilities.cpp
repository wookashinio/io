//
// Created by viktor on 12.05.2020.
//

#include "Utilities.h"

Vec2 Utilities::dynamicPosition(Vec2 start, int time, Vec2 velocity, float friction) {
  int curr_time = time;
  Vec2 curr_vel = velocity;
  Vec2 position = start;

  while (time != 0 && velocity.length() != 0) {
    if (ball_touched) { return Vec2(UNDEFINED, UNDEFINED); };
    position += velocity;
    velocity *= friction;
    time--;
  }

  while (position.x - PLAYER_R < 0 || position.x + PLAYER_R > FIELD_WIDTH) {
    if (position.x - PLAYER_R < 0) {
      position.x *= -1;
    } else {
      position.x = 2 * FIELD_WIDTH - position.x;
    }
  }

  while (position.y - PLAYER_R < 0 || position.y + PLAYER_R > FIELD_HEIGHT) {
    if (position.y - PLAYER_R < 0) {
      position.y *= -1;
    } else {
      position.y = 2 * FIELD_HEIGHT - position.y;
    }
  }

  return position;
}

Vec2 Utilities::shootPosition(Vec2 ball, Side side) {
  int sign;
  double angle, step, a, b, blank;

  blank = (FIELD_WIDTH - GOAL_WIDTH) / 2;

  if (side == N) {
    sign = 1;
    b = ball.y;
  } else {
    sign = -1;
    b = FIELD_HEIGHT - ball.y;
  }

  if (ball.x < (float) FIELD_WIDTH / 2) {
    step = 10;
  } else {
    step = -10;
  }

  if (ball.x > blank && ball.x < FIELD_WIDTH - blank) {
    return Vec2(ball.x, ball.y + BALL_R + 1);
  }

  angle = 90 + step;

  while (angle != 90 && angle != -90) {
    a = b * tan((M_PI * angle) / 180);
    a = (a + ball.x);

    a = Utilities::colisionCheck(a);

    if (a + ball.x > blank && a + ball.x < FIELD_WIDTH - blank) {
      return Vec2(ball.x + sin(sign * (M_PI * angle) / 180) + 1, ball.y + cos(sign * (M_PI * angle) / 180) + 1);
    }

    angle += step;
  }

  return Vec2(ball.x, ball.y + BALL_R + 1);
}

Vec2 Utilities::defensePosition(Vec2 ball, Side side) {
  Vec2 goal;

  if (side == N) {
    goal = Vec2((float) FIELD_HEIGHT / 2, 0);
  } else {
    goal = Vec2((float) FIELD_WIDTH / 2, FIELD_HEIGHT);
  }

  goal = Vec2((goal.x + ball.x) / 2, (goal.y + ball.y) / 2);

  if (goal.y + PLAYER_R > FIELD_HEIGHT) {
    goal.y = FIELD_HEIGHT - PLAYER_R;
  } else if (goal.y < PLAYER_R) {
    goal.y = PLAYER_R;
  }

  return goal;
}