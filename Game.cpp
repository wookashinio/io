//
// Created by kuba on 02.05.20.
//

#include "Game.h"
bool LeftPressed = false, RightPressed = false, DownPressed = false, UpPressed = false, SpacePressed = false;
bool WPressed = false, APressed = false, SPressed = false, DPressed = false, LControlPressed = false;
void EventHandler(sf::Event event) {


    if (event.type == sf::Event::EventType::KeyPressed) {
        switch (event.key.code) {
            case sf::Keyboard::Left: {

                if(!LeftPressed)
                    printf("d - left\n");

                LeftPressed = true;
                break;
            }
            case sf::Keyboard::Right: {

                if(!RightPressed)
                    printf("d - right\n");

                RightPressed = true;
                break;
            }
            case sf::Keyboard::Up: {
                if(!UpPressed)
                    printf("d - up\n");

                UpPressed = true;
                break;
            }
            case sf::Keyboard::Down: {
                if(!DownPressed)
                    printf("d - down\n");

                DownPressed = true;
                break;
            }
            case sf::Keyboard::Space: {

                if(!SpacePressed) {
                    printf("d - space\n");
                }
                SpacePressed = true;

                break;
            }
            case sf::Keyboard::W: {

                if(!WPressed) {
                    printf("d - w\n");
                }
                WPressed = true;

                break;
            }
            case sf::Keyboard::A: {

                if(!APressed) {
                    printf("d - a\n");
                }
                APressed = true;

                break;
            }
            case sf::Keyboard::S: {

                if(!SPressed) {
                    printf("d - s\n");
                }
                SPressed = true;

                break;
            }
            case sf::Keyboard::D: {

                if(!DPressed) {
                    printf("d - d\n");
                }
                DPressed = true;

                break;
            }
            case sf::Keyboard::LControl: {

                if(!LControlPressed) {
                    printf("d - LControl\n");
                }
                LControlPressed = true;

                break;
            }
        }
    } else if (event.type == sf::Event::EventType::KeyReleased) {
        switch (event.key.code) {
            case sf::Keyboard::Left: {
                LeftPressed = false;
                printf("u - left\n");
                break;
            }
            case sf::Keyboard::Right: {
                RightPressed = false;
                printf("u - right\n");
                break;
            }
            case sf::Keyboard::Up: {
                UpPressed = false;
                printf("u - up\n");
                break;
            }
            case sf::Keyboard::Down: {
                DownPressed = false;
                printf("u - down\n");
                break;
            }
            case sf::Keyboard::Space: {
                printf("u - space\n");
                SpacePressed = false;
                break;
            }
            case sf::Keyboard::W: {
                printf("u - W\n");
                WPressed = false;
                break;
            }
            case sf::Keyboard::A: {
                printf("u - A\n");
                APressed = false;
                break;
            }
            case sf::Keyboard::S: {
                printf("u - S\n");
                SPressed = false;
                break;
            }
            case sf::Keyboard::D: {
                printf("u - D\n");
                DPressed = false;
                break;
            }
          case sf::Keyboard::LControl: {
            printf("u - LControl\n");
            LControlPressed = false;
            break;
          }
        }

    }

}

void Game::addBot(Dummy &mover) {
  mover_vec.push_back(std::make_shared<Mover>(mover));
  players_number++;
  mover.play();
}

void Game::updateMovers(sf::RenderWindow& window) {
    for (int i = 0; i < players_number; ++i) {
        for (int j = i + 1; j < players_number; ++j) {
            mover_vec[i]->checkCollision(*mover_vec[j]);
        }
        mover_vec[i]->checkCollision(ball);
        mover_vec[i]->update();
        mover_vec[i]->checkBoundaryCollision(window);
        mover_vec[i]->draw(window);
    }
    ball.update();
    ball.checkBoundaryCollision(window);
    ball.draw(window);
}

void Game::play(sf::RenderWindow& window, sf::Sprite& sprite) {
    printf("%d\n", players_number);
    bool gameLock = false;
    while (window.isOpen())
    {
        sf::Event event{};
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();


            EventHandler(event);
        }

        if(clock.getElapsedTime().asSeconds() > 1.0/40.0) {

            updateMovers(window);

            clock.restart();
        }

        if(!gameLock) {
            if (RightPressed) {
                mover_vec[0]->applyForce({ACCELERATION, 0});
            }
            if (LeftPressed) {
                mover_vec[0]->applyForce({-ACCELERATION, 0});
            }
            if (DownPressed) {
                mover_vec[0]->applyForce({0, ACCELERATION});
            }
            if (UpPressed) {
                mover_vec[0]->applyForce({0, -ACCELERATION});
            }
            if(SpacePressed) {
                mover_vec[0]->kick(ball);
                SpacePressed = mover_vec[0]->kicking;
            } else {
                mover_vec[0]->kicking = false;
            }
            if (secPlayer) {
                if(LControlPressed) {
                    mover_vec[1]->kick(ball);
                    LControlPressed = mover_vec[0]->kicking;
                } else {
                    mover_vec[1]->kicking = false;
                }
                if (DPressed) {
                    mover_vec[1]->applyForce({ACCELERATION, 0});
                }
                if (APressed) {
                    mover_vec[1]->applyForce({-ACCELERATION, 0});
                }
                if (SPressed) {
                    mover_vec[1]->applyForce({0, ACCELERATION});
                }
                if (WPressed) {
                    mover_vec[1]->applyForce({0, -ACCELERATION});
                }
            }
        }

        window.clear();
        window.draw(sprite);

        goalN.draw(window);
        goalS.draw(window);

        bool goal = goalN.isGoal(ball);
        std::time_t currentTime = std::time(nullptr);

        if (currentTime - startTime >= maxTime && !gameLock) {
            gameLock = true;
            if (goalsN > goalsS) {
                printf("PLAYER N WON!\n");
            } else if (goalsN < goalsS) {
                printf("PLAYER S WON!\n");
            } else {
                printf("DRAW\n");
            }
        }

        if (goal) {
            printf("GOAL N!\n");
            goalsN++;
            if (goalsN == MAX_GOALS) {
                gameLock = true;
                printf("PLAYER N WON!\n");
                window.setTitle("PLAYER N WON!");
            }
        } else {
            goal = goalS.isGoal(ball);
            if (goal) {
                printf("GOAL S!\n");
                goalsS++;
                if (goalsS == MAX_GOALS) {
                    gameLock = true;
                    printf("PLAYER S WON!\n");
                    window.setTitle("PLAYER S WON!");
                }
            }
        }

        if (goal && !gameLock) {
           ball.resetToPosition(BALL_X, BALL_Y); 
           mover_vec[0]->resetToPosition(PLAYER_1_X, PLAYER_1_Y);
           mover_vec[1]->resetToPosition(PLAYER_2_X, PLAYER_2_Y);
           printf("PLAYER N %d : %d PLAYER S\n", goalsN, goalsS);
           std::string s = "PLAYER N " + std::to_string(goalsN) + " : " + 
            std::to_string(goalsS) + " PLAYER S";
           window.setTitle(s);
        }

        goal = false;

        updateMovers(window);

        window.display();
    }
}