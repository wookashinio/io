#include "Vec2.h"

float Vec2::length(){
    return sqrt(x*x + y*y);
}

Vec2 Vec2::normalize() {
    float l = length();
    return Vec2(x / l, y / l );
}

float Vec2::dot(Vec2 v) {
    return x*v.x + y*v.y;
}